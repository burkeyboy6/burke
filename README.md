#What Do You Mean
By: Matthew Burke
*****
Text can be interpreted, words can not. Look at the following statement

-Yeah, I had sooooooo much fun last night


When you read that, how did you read it? Like this? ![like this] (https://i.pinimg.com/736x/d3/61/e2/d361e2803b8b87ddd4d04c5a2665d71a--sarcastic-face-ay.jpg)
Or like this? ![or like this?] (https://i.pinimg.com/736x/a8/77/46/a877463c8ff55d5dbd531826356d319c--happy-baby-so-happy.jpg)

This is something that is unique to written text becuase two people can read something with the same words but get two different meanings from it.

According to Steven Shaviro, "Every connection has its price; the one you can be sure of is that, sooner rather or later you will have to pay."
-what i took away from this is that although these two things have the same connection (trying to get people information), they have different prices. This means that they each have things that they do well and things that they do not